import sqlalchemy as sa
from sqlalchemy.sql import text
from sqlalchemy.ext.declarative import declarative_base
import pandas as pd

engine = sa.create_engine("postgresql+psycopg2://root:root@localhost:5432/test_db")

Base = declarative_base()

metadata = sa.MetaData(engine)
metadata.reflect()
Session = sa.orm.sessionmaker(engine)
session = Session()
update_mapper = {
    'disease': lambda disease_code, pathogen, description, id:f"""
        UPDATE Disease SET pathogen='{pathogen}', description='{description}',
                                                id={id}
                                                WHERE disease_code='{disease_code}';
    """,
    'diseasetype': lambda id, description: f"""
    UPDATE DiseaseType SET description='{description}'
                                                WHERE id={id}; 
    """,
    'country': lambda cname, population: f"""
        UPDATE Country SET population={population}
                                                WHERE cname='{cname}'; 
    """,

    'discover': lambda cname, disease_code, first_enc_date: f"""
        UPDATE Discover SET first_enc_date={first_enc_date}
                                                WHERE cname='{cname}'
                                                AND disease_code='{disease_code}'; 
    """,

    'users': lambda email, name, surname, salary, phone, cname: f"""
       UPDATE Users SET name='{name}', surname='{surname}', salary={salary},
                        phone='{phone}', cname='{cname}'
                                               WHERE email='{email}'; 
   """,

    'publicservant': lambda email, department: f"""
        UPDATE PublicServant SET department='{department}'
                                               WHERE email='{email}'; 
    """,

    'doctor': lambda email, degree: f"""
        UPDATE Doctor SET degree={degree}
                                        WHERE email='{email}'; 
    """,
    'specialize': lambda x: x,

    'record': lambda email, cname, disease_code, total_deaths, total_patients:
                f"""
                 UPDATE Record SET total_deaths={total_deaths}, total_patients={total_patients}
                                               WHERE email='{email}' AND cname='{cname}' AND disease_code='{disease_code}';
                
                """
}

delete_mapper = {
    'disease': lambda disease_code, pathogen, description, id: f"""
     DELETE FROM Disease WHERE disease_code='{disease_code}';
 """,
    'diseasetype': lambda id, description: f"""
    DELETE FROM DiseaseType WHERE id={id}; 
 """,
    'country': lambda cname, population: f"""
     DELETE FROM Country WHERE cname='{cname}'; 
 """,
    'discover': lambda cname, disease_code, first_enc_date: f"""
     DELETE FROM Discover WHERE cname='{cname}'
                                             AND disease_code='{disease_code}'; 
 """,

    'users': lambda email, name, surname, salary, phone, cname: f"""
    DELETE FROM Users WHERE email='{email}'; 
""",

    'publicservant': lambda email, department: f"""
     DELETE FROM PublicServant WHERE email='{email}'; 
 """,

    'doctor': lambda email, degree: f"""
     DELETE FROM Doctor WHERE email='{email}'; 
 """,
    'specialize': lambda id, email: f"""
     DELETE FROM Specialize WHERE email='{email} AND id={id}'""",
    'record': lambda email, cname, disease_code, total_deaths, total_patients:
    f"""
              DELETE FROM Record WHERE email='{email}' AND cname='{cname}' AND disease_code='{disease_code}';

             """
}
insert_mapper = {
    'disease': lambda disease_code, pathogen, description, id: f"""
     INSERT INTO Disease VALUES ('{disease_code}', '{pathogen}', '{description}', '{id}');
 """,
    'diseasetype': lambda id, description: f"""
    INSERT INTO DiseaseType(description) VALUES ('{description}'); 
 """,
    'country': lambda cname, population: f"""
     INSERT INTO Country VALUES ('{cname}', {population}); 
 """,
    'discover': lambda cname, disease_code, first_enc_date: f"""
     INSERT INTO Discover VALUES ('{cname}', '{disease_code}', {first_enc_date}); 
 """,

    'users': lambda email, name, surname, salary, phone, cname: f"""
    INSERT INTO Users VALUES ('{email}', '{name}', '{surname}', {salary}, '{phone}', '{cname}'); 
""",

    'publicservant': lambda email, department: f"""
     INSERT INTO PublicServant VALUES ('{email}', '{department}'); 
 """,

    'doctor': lambda email, degree: f"""
     INSERT INTO Doctor VALUES ('{email}', '{degree}'); 
 """,
    'specialize': lambda id, email: f"""
     INSERT INTO Specialize VALUES ({id}, '{email}')
     """,

    'record': lambda email, cname, disease_code, total_deaths, total_patients:
    f"""
             INSERT INTO Record VALUES ('{email}', '{cname}', '{disease_code}', {total_deaths}, {total_patients});

             """
}


def get_all(tablename):
    with engine.connect() as conn:
        q = text(f"""SELECT *  FROM {tablename}""")
        data = pd.DataFrame.from_records(conn.execute(q).fetchall(), columns=get_cols(tablename))
    return data


def get_cols(tablename):
    with engine.connect() as conn:
        cols = conn.execute(f"""SELECT
                                    column_name 
                                FROM
                                    information_schema.columns
                                WHERE
                                    table_name='{tablename}'
                                     AND table_schema = 'public'""").fetchall()
    return [col[0] for col in cols]


def get_tables():
    with engine.connect() as conn:
        tabs = conn.execute(f"""SELECT
                                    distinct table_name 
                                FROM
                                    information_schema.columns
                                WHERE
                                table_schema = 'public'""").fetchall()
        return [tab[0] for tab in tabs]

def get_top_by_deaths(k):
    with engine.connect() as conn:
        return conn.execute(f"""
            SELECT cname, total FROM (SELECT cname, sum(total_deaths) as total FROM Record group by cname order by total desc limit {k}) as t
        """).fetchall()


def get_top_by_total(k):
    with engine.connect() as conn:
        return conn.execute(f"""
            SELECT cname, total FROM (SELECT cname, sum(total_patients) as total FROM Record group by cname order by total desc limit {k}) as t
        """).fetchall()


def get_top_by_treated(k):
    with engine.connect() as conn:
        return conn.execute(f"""
            SELECT cname, total FROM (SELECT cname, sum(total_patients - total_deaths) as total FROM Record group by cname order by total desc limit {k}) as t
        """).fetchall()


def upd_table(tablename, r):
    with engine.connect() as conn:
        return conn.execute(update_mapper[tablename](*r)).rowcount


def del_from_table(tablename, r):
    with engine.connect() as conn:
        return conn.execute(delete_mapper[tablename](*r))


def insert_to_table(tablename, r):
    with engine.connect() as conn:
        return conn.execute(insert_mapper[tablename](*r))


def get_disease_types():
    with engine.connect() as conn:
        return conn.execute("SELECT * FROM DiseaseType").fetchall()


def get_countries():
    with engine.connect() as conn:
        return conn.execute("SELECT cname FROM Country").fetchall()


def get_disease_codes():
    with engine.connect() as conn:
        return conn.execute("SELECT disease_code FROM Disease").fetchall()


def get_users_not_servants():
    with engine.connect() as conn:
        return conn.execute("SELECT email FROM Users where email not in (SELECT email from PublicServant)").fetchall()


def get_users_not_doctors():
    with engine.connect() as conn:
        return conn.execute("SELECT email FROM Users where email not in (SELECT email from Doctor)").fetchall()


def get_doctors():
    with engine.connect() as conn:
        return conn.execute("SELECT email FROM Doctor ").fetchall()


def get_servants():
    with engine.connect() as conn:
        return conn.execute("SELECT email FROM PublicServant ").fetchall()



if __name__ == "__main__":
    print(get_all("disease"))
    print(get_cols('disease'))
    print(get_tables())
    print(get_disease_types())
