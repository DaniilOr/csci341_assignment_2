DROP TABLE IF EXISTS DiseaseType;
DROP TABLE IF EXISTS Country;
DROP TABLE IF EXISTS Disease;
DROP TABLE IF EXISTS Discover;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS PublicServant;
DROP TABLE IF EXISTS Doctor;
DROP TABLE IF EXISTS Specialize;
DROP TABLE IF EXISTS Record;



CREATE TABLE DiseaseType (
     id BIGSERIAL PRIMARY  KEY,
     description VARCHAR(140)
);

CREATE TABLE Country (
     cname VARCHAR(50) PRIMARY  KEY,
     population BIGINT
);
CREATE TABLE Disease (
    disease_code VARCHAR(50) PRIMARY  KEY,
    pathogen VARCHAR(20),
    description VARCHAR(140),
    id BIGINT REFERENCES DiseaseType(id)
);

CREATE TABLE Discover(
    cname VARCHAR(50) REFERENCES Country(cname) ON UPDATE CASCADE ON DELETE CASCADE,
    disease_code VARCHAR(50) REFERENCES Disease(disease_code) ON UPDATE CASCADE ON DELETE CASCADE,
    first_enc_date DATE,
    PRIMARY KEY (cname, disease_code)
);

CREATE TABLE Users(
    email VARCHAR(60) PRIMARY KEY,
    name VARCHAR(30),
    surname VARCHAR(40),
    salary INTEGER,
    phone VARCHAR(20),
    cname VARCHAR(50) REFERENCES Country(cname) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PublicServant(
    email VARCHAR(60) PRIMARY KEY REFERENCES Users(email) ON DELETE CASCADE ON UPDATE CASCADE,
    department VARCHAR(60)
);

CREATE TABLE Doctor(
    email VARCHAR(60) PRIMARY KEY REFERENCES Users(email) ON DELETE CASCADE ON UPDATE CASCADE,
    degree VARCHAR(20)
);

CREATE TABLE Specialize(
    id BIGSERIAL REFERENCES DiseaseType(id) ON DELETE CASCADE ON UPDATE CASCADE,
    email VARCHAR(60) REFERENCES Doctor(email) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(id, email)
);

CREATE TABLE Record(
    email VARCHAR(60) REFERENCES PublicServant(email) ON DELETE CASCADE ON UPDATE CASCADE,
    cname VARCHAR(50) REFERENCES Country(cname) ON DELETE CASCADE ON UPDATE CASCADE,
    disease_code VARCHAR(50) REFERENCES Disease(disease_code) ON DELETE CASCADE ON UPDATE CASCADE,
    total_deaths INTEGER,
    total_patients INTEGER,
    PRIMARY KEY(email, cname, disease_code)
);


