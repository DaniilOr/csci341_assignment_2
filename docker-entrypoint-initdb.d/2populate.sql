INSERT INTO Country VALUES      ('Kazakhstan',18776707),
                                ('Russia',145934462),
                                ('England',67886011),
                                ('Zambia',18383955),
                                ('USA',331002651),
                                ('Germany',83783942),
                                ('Congo',5518087),
                                ('Greece',212559417),
                                ('China',1439323776),
                                ('Tanzania',59734218),
                                ('Spain',46754778);

INSERT INTO DiseaseType VALUES (1, 'virology'),
                                            (2, 'deficiency disease'),
                                            (3, 'hereditary disease'),
                                            (4, 'physiological disease'),
                                            (5, 'bacterial disease'),
                                            (6, 'fungal disease'),
                                            (7, 'parasitic disease'),
                                            (8, 'autoimmune diseases'),
                                            (9, 'cancer'),
                                            (10, 'infectious disease');

INSERT INTO Disease VALUES ('tuberculosis', 'bacteria', 'a potentially serious infectious disease that mainly affects the lungs', 5),
                           ('pneumonia', 'bacteria', 'an infection that inflames the air sacs in one or both lungs', 5),
                           ('syphilis', 'bacteria', 'a STI that can cause serious health problems without treatment.', 5),
                           ('covid-19', 'virus', 'a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2)', 1),
                           ('ascariasis', 'parasite', 'the most common human helminthic infection globally', 7),
                           ('sarcoma', 'virus', 'a broad group of cancers that begin in the soft (also called connective) tissues', 9),
                           ('candidiasis', 'fungi', 'a fungal infection due to any type of Candida (a type of yeast)', 6),
                           ('meningitis', 'virus', 'an inflammation (swelling) of the protective membranes covering the brain and spinal cord', 1),
                           ('ebola', 'virus', 'caused by Ebola virus, a member of the filovirus family', 1),
                           ('spanish flu', 'virus', 'caused by the H1N1 influenza', 1);

INSERT INTO Discover VALUES ('Germany', 'tuberculosis', '1882-03-24'),
                            ('Germany', 'pneumonia', '1930-05-25'),
                            ('Germany', 'syphilis', '1930-06-26'),
                            ('China', 'covid-19', '2019-11-17'),
                            ('England', 'ascariasis', '1855-03-01'),
                            ('USA', 'sarcoma', '1966-11-21'),
                            ('Greece', 'candidiasis', '1911-11-11'),
                            ('England', 'meningitis', '1661-08-07'),
                            ('Congo', 'ebola', '1976-05-23'),
                            ('USA', 'spanish flu', '1918-04-17');

INSERT INTO Users VALUES('max.plank@google.com', 'max', 'plank', 100000, '88005553535', 'Germany'),
                         ('john.doe@google.com', 'john', 'doe', 200000, '88005553533', 'USA'),
                         ('alibek.abeuov@google.com', 'alibek', 'abeuov', 10000, '123-456', 'Kazakhstan'),
                         ('beksultan.kozha@google.com', 'beksultan', 'kozha', 400000, '456-123', 'Kazakhstan'),
                         ('gulsim.zhanat@google.com', 'gulsim', 'zhanat', 1234567, '11-22-33', 'Kazakhstan'),
                         ('asemgul.zhibek@google.com', 'asemgul', 'zhibek', 250000, '88005653535', 'Kazakhstan'),
                         ('peter.roshe@google.com', 'peter', 'roshe', 100001, '88005573535', 'England'),
                         ('liza.granz@google.com', 'liza', 'granz', 125000, '88005553535', 'Greece'),
                         ('ivan.ivanov@google.com', 'ivan', 'ivanov', 100000, '88005554535', 'Russia'),
                         ('jose.johnson@google.com', 'jose', 'johnson', 350000, '88005593535', 'Spain'),
                         ('masiye.suzgho@google.com', 'masiye', 'suzgho', 1030000, '88005553585', 'Zambia'),
                         ('daniil.orel@nu.edu.kz', 'daniil', 'orel', 10000000, '12', 'Kazakhstan'),

                         ('dana.brown@google.com','dana','brown',630000,'4534715134','China'),
                         ('helen.helen@google.com','helen','helen',630000,'4534716134','England'),
                         ('james.white@google.com','james','white',21000,'62306862','USA'),
                         ('ming.ty@google.com','ming','ty',780000,'7765270229','China'),
                         ('ian.power@google.com','ian','power',1200000,'5384801118','Greece'),
                         ('john.stark@google.com', 'john', 'stark', 99999, '737722', 'Congo'),
                         ('nathan.dave@google.com','nathan','dave',103000,'6969696','Tanzania'),
                         ('rabina.gulim@google.com', 'rabina', 'gulim', 123456, '222-333', 'Kazakhstan'),
                         ('dan.hash@google.com', 'dan', 'hash', 654321, '333-444', 'Congo'),
                         ('semen.ivanov@google.com', 'semen', 'ivanov', 444444, '444-444', 'Russia');

INSERT INTO PublicServant VALUES ('dana.brown@google.com', 'Department 1'),
                                 ('helen.helen@google.com', 'Department 2'),
                                 ('james.white@google.com', 'Department 3'),
                                 ('ming.ty@google.com', 'Department 1'),
                                 ('ian.power@google.com', 'Department 2'),
                                 ('john.stark@google.com', 'Department 3'),
                                 ('nathan.dave@google.com', 'Department 4'),
                                 ('rabina.gulim@google.com', 'Department 1'),
                                 ('dan.hash@google.com', 'Department 2'),
                                 ('semen.ivanov@google.com', 'Department 4');

INSERT INTO Doctor VALUES ('masiye.suzgho@google.com', 'phd'),
                          ('jose.johnson@google.com', 'phd'),
                          ('ivan.ivanov@google.com', 'associate'),
                          ('liza.granz@google.com', 'phd'),
                          ('peter.roshe@google.com', 'associate'),
                          ('asemgul.zhibek@google.com', 'phd'),
                          ('gulsim.zhanat@google.com', 'phd'),
                          ('beksultan.kozha@google.com', 'phd'),
                          ('alibek.abeuov@google.com', 'phd'),
                          ('john.doe@google.com', 'phd'),
                          ('daniil.orel@nu.edu.kz', 'bachelor'),
                          ('max.plank@google.com', 'phd');

INSERT INTO Specialize VALUES (1, 'masiye.suzgho@google.com'),
                              (1, 'ivan.ivanov@google.com'),
                              (1, 'peter.roshe@google.com'),
                              (1, 'max.plank@google.com'),
                              (1, 'beksultan.kozha@google.com'),
                              (1, 'gulsim.zhanat@google.com'),
                              (1, 'asemgul.zhibek@google.com'),
                              (2, 'asemgul.zhibek@google.com'),
                              (2, 'alibek.abeuov@google.com'),
                              (2, 'liza.granz@google.com'),
                              (10, 'john.doe@google.com'),
                              (10, 'max.plank@google.com'),
                              (3, 'jose.johnson@google.com'),
                              (3, 'peter.roshe@google.com'),
                              (4, 'gulsim.zhanat@google.com'),
                              (5, 'masiye.suzgho@google.com'),
                              (8, 'alibek.abeuov@google.com'),
                              (3, 'max.plank@google.com'),
                              (5, 'max.plank@google.com'),
                              (4, 'jose.johnson@google.com'),
                              (1,'daniil.orel@nu.edu.kz'),
                              (10,'daniil.orel@nu.edu.kz'),
                              (6, 'alibek.abeuov@google.com');


INSERT INTO Record VALUES ('dana.brown@google.com', 'China', 'covid-19', 10, 100),
                          ('nathan.dave@google.com', 'China', 'covid-19', 20, 200),
                          ('ming.ty@google.com', 'China', 'covid-19', 30, 300),
                          ('ming.ty@google.com', 'Greece', 'covid-19', 300, 350),
                          ('ming.ty@google.com', 'Russia', 'covid-19', 400, 450),
                          ('ming.ty@google.com', 'USA', 'covid-19', 5, 50),

                          ('dan.hash@google.com', 'Spain', 'spanish flu', 120000, 800000),
                          ('dan.hash@google.com', 'Greece', 'spanish flu', 10000, 800000),
                          ('semen.ivanov@google.com', 'China', 'covid-19', 125000, 620000),
                          ('ming.ty@google.com', 'USA', 'pneumonia', 700, 1100),
                          ('john.stark@google.com', 'USA', 'syphilis', 22, 222),
                          ('helen.helen@google.com', 'China', 'covid-19', 100, 1000),
                          ('helen.helen@google.com', 'USA', 'covid-19', 150, 1500),
                          ('helen.helen@google.com', 'Germany', 'covid-19', 200, 2000),
                          ('helen.helen@google.com', 'Greece', 'covid-19', 1000, 10000),
                          ('ian.power@google.com', 'Germany', 'meningitis', 100001, 100001),
                          ('ian.power@google.com', 'Russia', 'covid-19', 1000001, 10000001),
                          ('ian.power@google.com', 'Germany', 'covid-19', 1000001, 10000001);

--
-- INSERT INTO DiseaseType(id, description) VALUES (1, 'virology');
-- INSERT INTO DiseaseType(id, description) VALUES (2, 'infectious disease');
-- INSERT INTO DiseaseType(id, description) VALUES (3, 'respiratory');
-- INSERT INTO DiseaseType(id, description) VALUES (4, 'metabolic');
-- INSERT INTO DiseaseType(id, description) VALUES (5, 'deficiency');
-- INSERT INTO DiseaseType(id, description) VALUES (6, 'dermatological');
-- INSERT INTO DiseaseType(id, description) VALUES (7, 'cardiovascular');
-- INSERT INTO DiseaseType(id, description) VALUES (8, 'cancer');
-- INSERT INTO DiseaseType(ID, DESCRIPTION) VALUES (9, 'neurological');
-- INSERT INTO DiseaseType(id, description) VALUES (10, 'renal');
--
-- /*Insert into country*/
-- INSERT INTO Country(cname, population) VALUES ('Kazakhstan', 18200000);
-- INSERT INTO Country(cname, population) VALUES ('China', 1700000000);
-- INSERT INTO Country(cname, population) VALUES ('Russia', 19500000);
-- INSERT INTO Country(cname, population) VALUES ('UK', 4000400);
-- INSERT INTO Country(cname, population) VALUES ('Canada', 20000000);
-- INSERT INTO Country(cname, population) VALUES ('India', 1500000000);
-- INSERT INTO Country(cname, population) VALUES ('South Korea', 13000000);
-- INSERT INTO Country(cname, population) VALUES ('Japan', 10500000);
-- INSERT INTO Country(cname, population) VALUES ('Italy', 14500000);
-- INSERT INTO Country(cname, population) VALUES ('Chile', 19000000);
--
-- /*Insert into disease*/
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('covid-19', 'virus', 'bat thing', 1);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('core-17', 'bacteria', 'first disease', 2);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('mism', 'bacteria', 'second disease', 2);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('nur', 'fungi', 'third', 4);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('miruly', 'protist', 'fourth', 5);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('nani', 'virus', 'sixth', 6);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('woof', 'bacteria', 'seventh', 7);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('uwu', 'fungi', 'eighth', 8);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('mua', 'bacteria', 'ninth', 9);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('blabla', 'fungi', 'tenth', 10);
-- INSERT INTO Disease(disease_code, pathogen, description, id) VALUES ('ecat', 'virus', 'necessary', 3);
--
-- /*Insert into discover*/
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Kazakhstan', 'woof', '2021-05-21');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Kazakhstan', 'covid-19', '2020-10-10');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('UK', 'covid-19', '2020-05-25');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Canada', 'covid-19', '2020-01-20');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Chile','mua', '1986-01-05');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('South Korea', 'mism', '2006-06-05');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Japan', 'uwu', '2011-09-06');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Italy', 'blabla', '1984-05-04');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('China', 'nur', '1952-08-29');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('India', 'core-17', '2017-12-03');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Russia', 'nani', '2014-10-18');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Russia', 'miruly', '1920-11-12');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Russia', 'covid-19', '2020-11-11');
-- INSERT INTO Discover(cname, disease_code, first_enc_date) VALUES ('Italy', 'ecat', '1948-04-09');
--
--
-- /*Insert to users*/
-- INSERT INTO Users(email, name, surname, salary, phone, cname) VALUES ('srour@gmail.com','Kemhebek','Magjanev',5000,'7143709398','Kazakhstan'),
-- ('purvis@att.net','Erzhan','Beibitzhanov',5000,'8385355437','China'),
-- ('rwelty@mac.com','Marat','Yerzhanev',5000,'4072059458','Russia'),
-- ('burniske@outlook.com','Erzhan','Batyrkhanov',5000,'3763717520','Canada'),
-- ('dburrows@verizon.net','Almas','Dastanov',5000,'6234127852','South Korea'),
-- ('morain@live.com','Kemhebek','Azamatev',7000,'2964043279','Japan'),
-- ('marcs@icloud.com','Aida','Assylova',7000,'3107650473','Chile'),
-- ('scitext@gmail.com','Alibi','Camranov',7000,'7806929900','Italy'),
-- ('kmself@live.com','Yerbol','Bakhytzhanov',7000,'2238154876','UK'),
-- ('isaacson@yahoo.com','Daniyarbek','Jeanev',7000,'7424393185','India'),
-- ('mhoffman@sbcglobal.net','Dauren','Nurbolatev',8000,'2235706117','Kazakhstan'),
-- ('oechslin@mac.com','Kuanysh','Camranev',8000,'3223111977','China'),
-- ('phizntrg@mac.com','Nazira','Temirzhaneva',8000,'2972172610','Russia'),
-- ('gfxguy@att.net','Jaina','Assylhanova',8000,'5233303742','Canada'),
-- ('phyruxus@aol.com','Botakoz','Yerbolova',8000,'6664578589','South Korea'),
-- ('natepuri@outlook.com','Asemgul','Assyleva',10000,'4848820967','Japan'),
-- ('josephw@mac.com','Rita','Ershatova',10000,'3162915488','Chile'),
-- ('pizza@optonline.net','Asel','Bolatova',10000,'7007029530','Italy'),
-- ('uqmcolyv@hotmail.com','Sayagul','Daniyarova',10000,'9118930490','UK'),
-- ('cparis@me.com','Alibek','Tahirev',10000,'4535936717','India');
--
-- /*PublicServant*/
-- INSERT INTO PublicServant(email, department) VALUES ('srour@gmail.com','Dept1'),
-- ('purvis@att.net','Dept1'),
-- ('rwelty@mac.com','Dept1'),
-- ('burniske@outlook.com','Dept2'),
-- ('dburrows@verizon.net','Dept2'),
-- ('morain@live.com','Dept2'),
-- ('marcs@icloud.com','Dept3'),
-- ('scitext@gmail.com','Dept3'),
-- ('kmself@live.com','Dept3'),
-- ('isaacson@yahoo.com','Dept3');
--
-- /*Doctor*/
-- INSERT INTO Doctor(email, degree) VALUES ('mhoffman@sbcglobal.net','PhD'),
-- ('oechslin@mac.com','PhD'),
-- ('phizntrg@mac.com','PhD'),
-- ('gfxguy@att.net','PhD'),
-- ('phyruxus@aol.com','PhD'),
-- ('natepuri@outlook.com','Masters'),
-- ('josephw@mac.com','Masters'),
-- ('pizza@optonline.net','Masters'),
-- ('uqmcolyv@hotmail.com','Masters'),
-- ('cparis@me.com','Masters')	;
--
-- /*Insert into specialize*/
-- INSERT INTO Specialize(id, email) VALUES ('1','mhoffman@sbcglobal.net'),
-- ('2','oechslin@mac.com'),
-- ('3','phizntrg@mac.com'),
-- ('4','gfxguy@att.net'),
-- ('5','phyruxus@aol.com'),
-- ('6','natepuri@outlook.com'),
-- ('7','josephw@mac.com'),
-- ('8','pizza@optonline.net'),
-- ('9','uqmcolyv@hotmail.com'),
-- ('10','cparis@me.com'),
-- ('2','cparis@me.com'),
-- ('1','cparis@me.com');
--
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('srour@gmail.com','Kazakhstan','woof',105,456);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('purvis@att.net','China','nur',2005,6520);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('rwelty@mac.com','Russia','miruly',102,653);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('burniske@outlook.com','Chile','mua',456,6532);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('dburrows@verizon.net','South Korea','mism',895,5230);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('morain@live.com','Japan','uwu',8523,10022);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('marcs@icloud.com','Russia','nani',6532,6535);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('scitext@gmail.com','Italy','blabla',453,522);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('kmself@live.com','India','core-17',6532,65210);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('isaacson@yahoo.com','Canada','covid-19',6532,65320);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('isaacson@yahoo.com','UK','covid-19',2654,26540);
-- INSERT INTO Record(email, cname, disease_code, deaths, total_patients) VALUES ('isaacson@yahoo.com','Kazakhstan','covid-19',6520,65200),
-- ('isaacson@yahoo.com','Russia','covid-19',2535,25350),
-- ('kmself@live.com','Italy','ecat',4532,85421);