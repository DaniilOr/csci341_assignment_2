-- -- Q1:
-- SELECT t1.disease_code as disease_code,
-- 		t1.description as description FROM (SELECT disease_code, description FROM Disease
-- WHERE pathogen='bacteria') as t1 join
-- Discover as d on t1.disease_code = d.disease_code where d.first_enc_date < '1990-01-01';
--
-- -- Q2
SELECT t1.name as name, t1.surname as surname, t1.degree as degree
FROM (
 	SELECT name, surname, degree, d.email as email
	from Doctor as d join Users as u on d.email = u.email)
as t1 Where t1.email not in (
SELECT email FROM Specialize where id  in
	(SELECT id from DiseaseType where description = 'infectious disease')
);
-- -- Q3
-- SELECT name, surname, degree FROM
-- (SELECT name, surname, degree from Users as u join Doctor as d on u.email=d.email where u.email in
--  (SELECT email FROM Specialize group by email having count(*) > 2) ) as t1;
--
-- -- Q4
-- SELECT c.cname as cname, avg(u.salary) as avg_salary
-- 	From Country as c join (SELECT * from Users where email in
-- 							(SELECT email from Specialize where id in
-- 							                                    (SELECT id from DiseaseType where description='virology')
-- 							    )
-- 	    )as u on c.cname = u.cname group by c.cname;
--
-- -- Q5
-- SELECT department, count(*) as num_servants FROM ((SELECT email FROM Record where disease_code='covid-19' group by email having count(*) > 1)
-- as r JOIN PublicServant AS p ON r.email=p.email) group by department;
-- -- Q6
-- UPDATE Users SET salary=2*salary WHERE email IN (SELECT email FROM Record
-- 												 where disease_code='covid-19' group by
-- 												 email having count(*)>3);

-- -- Q7
-- DELETE FROM Users where name like '%bek%' or name like '%gul%';
--
-- -- Q8
-- CREATE INDEX idx_pathogen
-- ON Disease (pathogen);
--
-- -- Q9
-- SELECT u.email as email, name, department FROM Users as u Join (SELECT * FROM PublicServant WHERE
-- 							email in (SELECT email FROM Record WHERE total_patients>=100000
-- 									 and total_patients<=999999)) as ps on u.email=ps.email;
-- -- Q10
-- SELECT cname FROM (SELECT cname, sum(total_patients) as total FROM Record)
--
-- -- Q11
-- SELECT d.description as type_of_disease, sum(r.total_patients - r.total_deaths) as treated_patients FROM (SELECT dt.description, d.disease_code FROM DiseaseType as dt JOIN Disease as d on dt.id=d.id)
-- as d
-- 	JOIN Record as r on r.disease_code=d.disease_code group by d.description;