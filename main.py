from sqlalchemy.sql import text
from sqlalchemy import create_engine

engine = create_engine("postgresql+psycopg2://root:root@localhost:5432/test_db")


if __name__ == '__main__':
    with engine.connect() as conn:
        print("List the disease code and the description of diseases that are caused by “bacteria” (pathogen) and were discovered before 1990:")
        question_1 = text("""SELECT t1.disease_code as disease_code,
		t1.description as description FROM (SELECT disease_code, description FROM Disease
        WHERE pathogen='bacteria') as t1 join
        Discover as d on t1.disease_code = d.disease_code where d.first_enc_date < '1990-01-01'""")
        print("\tResult: ", *conn.execute(question_1).fetchall(), sep='\n\t\t')

        print("List the name, surname and degree of doctors who are not specialized in “infectious diseases:.")
        question_2 = text("""
                        SELECT t1.name as name, t1.surname as surname, t1.degree as degree
            FROM (
                SELECT name, surname, degree, d.email as email
                from Doctor as d join Users as u on d.email = u.email)
            as t1 Where t1.email not in (
            SELECT email FROM Specialize where id  in
                (SELECT id from DiseaseType where description = 'infectious disease')
            )
        """)
        print("\tResult: ", *conn.execute(question_2).fetchall(), sep='\n\t\t')
        print("List the name, surname and degree of doctors who are specialized in more than 2 disease types:")
        question_3 = text("""
            SELECT name, surname, degree FROM
                (SELECT name, surname, degree from Users as u join Doctor as d on u.email=d.email where u.email in
                 (SELECT email FROM Specialize group by email having count(*) > 2) ) as t1;
        """)
        print("\tResult: ", *conn.execute(question_3).fetchall(), sep='\n\t\t')
        print("For each country, list the cname and average salary of doctors who are specialized in “virology”:")
        question_4 = text("""
        SELECT c.cname as cname, avg(u.salary) as avg_salary
	From Country as c join (SELECT * from Users where email in
							(SELECT email from Specialize where id in
							                                    (SELECT id from DiseaseType where description='virology')
							    )
	    )as u on c.cname = u.cname group by c.cname

        """)
        print("\tResult: ", *conn.execute(question_4).fetchall(), sep='\n\t\t')
        print("List the departments of public servants who report “covid-19” cases in more than one country and the number of such public servants who work in these departments. (i.e “Dept1 3” means that in the “Dept1” department there are 3 such employees.):")
        question_5 = text("""
                SELECT department, count(*) as num_servants FROM ((SELECT email FROM Record where disease_code='covid-19' group by email having count(*) > 1)
                    as r JOIN PublicServant AS p ON r.email=p.email) group by department;                    

        """)
        print("\tResult: ", *conn.execute(question_5).fetchall(), sep='\n\t\t')
        print("Double the salary of public servants who have recorded covid-19 patients more than 3 times:")
        print("\tSee the initial salaries")
        print("\tInitial values", *conn.execute(text("SELECT email, salary FROM Users LIMIT 100")).fetchall(), sep='\n\t\t')
        question_6 = text("""
            UPDATE Users SET salary=2*salary WHERE email IN (SELECT email FROM Record
                                                                     where disease_code='covid-19' group by
                                                                     email having count(*)>3)
        """)
        print("Affected rows:", conn.execute(question_6).rowcount)
        print("\tNow we make sure that the request worked correctly")
        print("\tResults after update", *conn.execute(text("SELECT email, salary FROM Users LIMIT 100")).fetchall(), sep='\n\t\t')
        print("Delete the users whose name contain the substring “bek” or “gul” (e.g. Alibek, Gulsim):")
        print("\tInitial users", *conn.execute(text("SELECT name FROM Users LIMIT 100")).fetchall(), sep='\n\t\t')
        question_7 = text("""
            DELETE FROM Users where name like '%bek%' or name like '%gul%';
        """)
        print(conn.execute(question_7).rowcount)
        print("\tUsers after deletion", *conn.execute(text("SELECT name FROM Users LIMIT 100")).fetchall(), sep='\n\t\t')

        print("Create an index namely “idx pathogen” on the “pathogen” field:")
        print("\tIndexes before addition:", conn.execute(text("""
            SELECT
                tablename,
                indexname,
                indexdef
            FROM
                pg_indexes
            WHERE
                schemaname = 'public'
                and tablename='disease';
        """)).fetchall(), sep='\n\t\t')
        question_8 = text("""
            CREATE INDEX IF NOT EXISTS idx_pathogen 
            ON Disease (pathogen) ;
        """)
        conn.execute(question_8)
        print("\tIndexes after addition:", *conn.execute(text("""
                   SELECT
                       tablename,
                       indexname,
                       indexdef
                   FROM
                       pg_indexes
                   WHERE
                       schemaname = 'public'
                       and tablename='disease';
               """)).fetchall(), sep='\n\t\t')

        print("List the email, name, and department of public servants who have created records where the number of patients is between 100000 and 999999:")

        question_9 = text("""
            SELECT u.email as email, name, department FROM Users as u Join (SELECT * FROM PublicServant WHERE
							email in (SELECT email FROM Record WHERE total_patients>=100000
									 and total_patients<=999999)) as ps on u.email=ps.email
        """)
        print("\tResult: ", *conn.execute(question_9).fetchall(), sep='\n\t\t')
        print("List the top 5 counties with the highest number of total patients recorded.:")
        question_10 = text("""
            SELECT cname, total FROM (SELECT cname, sum(total_patients) as total FROM Record group by cname order by total desc limit 5) as t
        """)
        print("\tResult: ", *conn.execute(question_10).fetchall(), sep='\n\t\t')
        print("Group the diseases by disease type and the total number of patients treated:")
        question_11 = text("""
            SELECT d.description as type_of_disease, sum(r.total_patients - r.total_deaths) as treated_patients FROM (SELECT dt.description, d.disease_code FROM DiseaseType as dt JOIN Disease as d on dt.id=d.id)
            as d
	        JOIN Record as r on r.disease_code=d.disease_code group by d.description
        """)
        print("\tResult: ", *conn.execute(question_11).fetchall(), sep='\n\t\t')
