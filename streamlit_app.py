from crud import *
import streamlit as st
from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, DataReturnMode

CRUD_OPTIONS = ['Read', 'Create', 'Update', 'Delete']


def add_row(table):
    df = pd.DataFrame(table['data'])
    new_row = [[_ for _ in range(len(df.columns))]]
    df_empty = pd.DataFrame(new_row, columns=df.columns)
    df = pd.concat([df, df_empty], axis=0, ignore_index=True)
    return df


SKIP = False


if __name__ == "__main__":
    with st.sidebar:
        command = st.selectbox('Mode:', CRUD_OPTIONS)

    option = st.selectbox("Pick the table:", get_tables())
    gd = GridOptionsBuilder.from_dataframe(get_all(option))
    gd.configure_pagination(enabled=True)
    gd.configure_default_column(editable=True, groupable=True)
    sel_mode = st.radio('Selection Type', options=['single', 'multiple'])
    gd.configure_selection(selection_mode=sel_mode, use_checkbox=True)
    gridoptions = gd.build()
    table = AgGrid(
            get_all(option),
            gridOptions=gridoptions,
            update_mode=GridUpdateMode.SELECTION_CHANGED,
            reload_data=True,
            data_return_mode=DataReturnMode.AS_INPUT,
        )
    if command == 'Read':
        k = st.number_input('Return top K:', min_value=1, max_value=100)
        totals = pd.DataFrame.from_records(get_top_by_total(k), columns=['country', 'total'])
        deaths = pd.DataFrame.from_records(get_top_by_deaths(k), columns=['country', 'deaths'])
        diff = pd.DataFrame.from_records(get_top_by_treated(k), columns=['country', 'treated'])
        row1_1, row1_2, row1_3 = st.columns(3)
        with row1_1:
            st.title('Total patients')
            st.bar_chart(totals, x='country', y='total')
        with row1_2:
            st.title('Total deaths')
            st.bar_chart(deaths, x='country', y='deaths')
        with row1_3:
            st.title('Total treated')
            st.bar_chart(diff, x='country', y='treated')
    elif command == 'Update':
        sel_row = table["selected_rows"]
        df_selected = pd.DataFrame(sel_row)
        if st.button('Update db (do no forget to flag the updated rows)', key=1):
            for i, r in df_selected.iterrows():
                updated_count = upd_table(option, r)
            st.write('Updated!')

    elif command == 'Delete':
        sel_row = table["selected_rows"]
        df_selected = pd.DataFrame(sel_row)
        if st.button('Delete row(s)', key=1):
            for i, r in df_selected.iterrows():
                del_from_table(option, r)
            st.write('Deleted!')
    elif command == "Create":
        if option == 'diseasetype':
            with st.form("Creating disease type"):
                desc = st.text_input("Enter description:")
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [desc])
        elif option == 'country':
            with st.form("Creating country"):
                cname = st.text_input("Enter country name:")
                population = st.number_input('Enter the population:', min_value=1, max_value=90000000)
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [cname,population])
        elif option == 'disease':
            with st.form("Creating disease"):
                code = st.text_input('Enter disease code:')
                pathogen = st.text_input('Enter pathogen:')
                desc = st.text_input('Enter description:')
                id_type_mapping = get_disease_types()
                id_type_mapping = {key: val for val, key in id_type_mapping}

                id = st.selectbox('Disease Type:', list(id_type_mapping.keys()))
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [code, pathogen,
                                             desc,id_type_mapping[id]])
        elif option == 'discover':
            with st.form("Creating discover"):
                first_enc_date = st.date_input('Enter the date:').strftime('YYYY-MM-DD')
                cname = st.selectbox('Country: ', [i[0] for i in get_countries()])
                disease_code = st.selectbox('Disease:', [i[0] for i in get_disease_codes()])
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [cname, disease_code,
                                             first_enc_date])
        elif option == 'users':
            with st.form("Creating user"):
                cname = st.selectbox('Country: ', [i[0] for i in get_countries()])
                email = st.text_input("Enter your email:")
                name = st.text_input("Enter your name:")
                surname = st.text_input("Enter your surname:")
                salary = st.number_input("Enter your salary:", min_value=0)
                phone = st.text_input("Enter your phone:")
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [email, name, surname,
                                             salary, phone, cname])
        elif option == 'publicservant':
            with st.form("Creating public servant"):
                email = st.selectbox("Email", [i[0] for i in get_users_not_servants()])
                department = st.text_input("Department:")
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [email, department])

        elif option == "doctor":
            with st.form("Creating doctor"):
                email = st.selectbox("Email:", [i[0] for i in get_users_not_doctors()])
                degree = st.text_input("Degree:")
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [email, degree])

        elif option == 'specialize':
            with st.form("Creating specialisation"):
                email = st.selectbox("Email:", [i[0] for i in get_doctors()])
                id_type_mapping = get_disease_types()
                id_type_mapping = {key: val for val, key in id_type_mapping}

                id = st.selectbox('Disease Type:', list(id_type_mapping.keys()))
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [id_type_mapping[id], email])

        elif option == "record":
            with st.form("Creating a record"):
                email = st.selectbox("Email:", [i[0] for i in get_servants()])
                cname = st.selectbox('Country: ', [i[0] for i in get_countries()])
                disease_code = st.selectbox('Disease:', [i[0] for i in get_disease_codes()])
                total_deaths = st.number_input("Deaths:", min_value=0)
                total_patients = st.number_input("Total patients:", min_value=0)
                submit = st.form_submit_button('Insert')
                if submit:
                    insert_to_table(option, [email, cname, disease_code, total_deaths, total_patients])
